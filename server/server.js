const server = require('http').createServer();
const io = require('socket.io')(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});
const uniqid = require('uuid').v1;
let users = [];
const channels = {};

function findUser(key){
  return users.find(item => {
    return item.data.id === key;
  });
}

function broadcast(type, msg, forcePure = false){
  users.forEach((user) => {
    if (typeof msg !== 'string' && !forcePure){
      msg = JSON.stringify(msg);
    }

    user.wsClient.emit(type, msg);
  })
}

io.on('connection', client => {
  const userKey = client.id;

  if(!users.find(item => item.data.id === userKey)) {
    users.push({
      wsClient: client,
      data: {id: userKey},
    })

    console.log(`user ${userKey} logged in`)

    client.emit('login', JSON.stringify({
      users: users.map((item)=>item.data),
      userId: userKey
    }));

    console.log('emitted login data');
  }

  client.on('nickname_setup', data => {
    data = data.replace(/\"/gm, '');

    const foundUser = findUser(userKey);
    foundUser.data.profileData = {nickname: data};
    foundUser.data.writing = false;

    users = users.map((item) => {
      if(item.data.id === userKey){
        return foundUser;
      }

      return item;
    })

    console.log(`Sending welcome party for: ${data}`)
    broadcast('welcome_party', foundUser.data)
  });

  client.on('chat_message', (data) => {
    data = JSON.parse(data);

    // console.log('msg', data)

    const targetUser = findUser(data.to.id);
//    console.log('msg-user', targetUser?.data)

    if(targetUser &&targetUser.wsClient) {
      console.log(`sending chat message ${data.msg.content} to ${targetUser.data.id}`)

      targetUser.wsClient.emit('chat_message', {
        channelKey: data.channelKey,
        msg: data.msg,
      });
    }
  })

  client.on('user_writing', (data) => {
    data = JSON.parse(data);
    const targetUser = findUser(data.to.id);

    if(targetUser &&targetUser.wsClient) {
      targetUser.wsClient.emit('user_writing', data.msg);
    }
  });

  client.on('disconnect', () => {
    console.log(`user ${userKey} disconnected`)
    const foundUser = findUser(userKey);
    broadcast('disconnected', foundUser.data);
    users = users.filter((item) => item.data.id !== userKey);
  });
});

const PORT = 3990;

console.log('Server starting on port ' + PORT)

io.listen(PORT);
