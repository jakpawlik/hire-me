class NicknameService {
  nickname!: string

  setNickname(name: string): void {
    this.nickname = name;
  }

  getNickname(): string | null {
    return this.nickname;
  }
}

export { NicknameService }
