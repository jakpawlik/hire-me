import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NicknameSetupComponent } from './nickname-setup.component';

describe('NicknameSetupComponent', () => {
  let component: NicknameSetupComponent;
  let fixture: ComponentFixture<NicknameSetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NicknameSetupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NicknameSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
