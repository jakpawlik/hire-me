import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import INickname from '../store/models/nickname.model'
import AppState from '../store/models/_state.model';
import {NicknameActionType} from "../store/actions/nickname.actions";
import { Router } from '@angular/router';
import {MsgService} from "../messenger/msg.service";
interface IForm {
  nickname: FormControl<string|null>
}

@Component({
  selector: 'app-nickname-setup',
  templateUrl: './nickname-setup.component.html',
  styleUrls: ['./nickname-setup.component.scss']
})
export class NicknameSetupComponent implements OnInit {
  theForm: FormGroup<IForm>;
  nickname$: Observable<INickname>;

  nickname: string = '';

  constructor(private formBuilder: FormBuilder, private store: Store<AppState>, private router: Router, private msgService: MsgService) {
    this.nickname$ = this.store.select((store) => store.nickname);

    this.theForm = this.formBuilder.group({
      nickname: '',
    });
  }

  goOn(): void {
    if(this.theForm.value){
      const val: string = this.theForm.value.nickname || '';

      this.theForm.reset();

      this.store.dispatch({ type: NicknameActionType.SET_NICKNAME, payload: {nickname: val}});

      window.localStorage.setItem('_nickname', val);

      this.msgService.reconnect();

      this.router.navigate(['messenger'])
    }
  }

  ngOnInit(): void {
    this.store.select(state => state.nickname)
      .subscribe((state) => {
        this.nickname = state.nickname || ''
      });
  }

}
