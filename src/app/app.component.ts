import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { Store } from '@ngrx/store';
import AppState from "./store/models/_state.model";
import {NicknameActionType} from "./store/actions/nickname.actions";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'HireMe Chat';
  nickname: string = '';

  constructor(private router: Router, private store: Store<AppState>) {
  }

  ngOnInit(){
    const savedNick = window.localStorage.getItem('_nickname');

    if(!!savedNick && savedNick !== ''){
      this.store.dispatch({ type: NicknameActionType.SET_NICKNAME, payload: {nickname: savedNick}});

      this.router.navigate(['messenger']);
    }

    this.store.select(state => state.nickname)
      .subscribe((state) => {
        this.nickname = state.nickname || ''
      });
  }
}
