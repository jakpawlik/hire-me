import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { GridModule, HeaderModule, FormModule, NavModule, ListGroupModule   } from '@coreui/angular';
import { NicknameSetupComponent } from './nickname-setup/nickname-setup.component';
import { MessengerComponent, ChatComponent, SidebarComponent } from './messenger/messenger.component';

import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { nicknameReducer } from './store/reducers/nickname.reducer';
import { IconModule, IconSetService } from '@coreui/icons-angular'
import {msgReducer} from "./store/reducers/msg.reducer";

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const wsConfig: SocketIoConfig = { url: 'ws://hire-me.papa.black:3990', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    NicknameSetupComponent,
    MessengerComponent,
    ChatComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GridModule,
    HeaderModule,
    ReactiveFormsModule,
    NavModule,
    IconModule,
    ListGroupModule,
    StoreModule.forRoot({ nickname: nicknameReducer, msg: msgReducer}),
    SocketIoModule.forRoot(wsConfig)
  ],
  providers: [
    IconSetService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
