import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NicknameSetupComponent} from './nickname-setup/nickname-setup.component'
import {MessengerComponent} from './messenger/messenger.component'

const routes: Routes = [
  { path: '', component: NicknameSetupComponent },
  { path: 'nickname-setup', component: NicknameSetupComponent },
  { path: 'messenger', component: MessengerComponent },
  { path: '**', component: NicknameSetupComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
