// import the interface
import IMsg from '../models/msg.model';
import { MsgActionType } from '../actions/messenging.actions';
import IUser from "../models/user.model";
import IChannels from "../models/channels.model";
import IChannel from "../models/channel.model";

export const initialState: IChannels = {
  channels: {},
  selectedChatUser: null
}

export function msgReducer(
  state: IChannels = initialState,
  action: { payload?: any, type: string}
): IChannels {

  switch (action.type) {
    case MsgActionType.MSG_SELECT_USER: {
      if(!action.payload){
        return state;
      }

      return { channels: {...state.channels}, selectedChatUser: action.payload};
    }
    case MsgActionType.MSG_WRITE: {
      if(!action.payload){
        return state;
      }

      const data = {channels: {...state.channels}, selectedChatUser: state.selectedChatUser}
      const channel = data.channels[action.payload.key];

      if(channel){
        const old = data.channels[action.payload.key].messages;
        data.channels[action.payload.key] = {
          id: action.payload.key,
          messages: [...old, action.payload.data]
        }
      }else{
        data.channels[action.payload.key] = {
          id: action.payload.key,
          messages: [action.payload.data]
        }
      }

      return data;
    }
    default:
      return state;
  }
}
