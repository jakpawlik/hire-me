// import the interface
import NicknameItem from '../models/nickname.model';
import { SetNicknameAction, NicknameActionType } from '../actions/nickname.actions';
import {Action} from "@ngrx/store";
import {TypedAction} from "@ngrx/store/src/models";

export const initialState: NicknameItem = {
  nickname: '',
}

export function nicknameReducer(
  state: NicknameItem = {
    nickname: ''
  },
  action: { payload?: NicknameItem, type: string}
): NicknameItem {

  switch (action.type) {
    case NicknameActionType.SET_NICKNAME:
      return {
        ...state,
        nickname: action.payload ? action.payload.nickname : ''
      }
    default:
      return state;
  }
}
