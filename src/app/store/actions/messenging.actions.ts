import {  createAction, props, ActionCreator } from '@ngrx/store';

import IUser from "../models/user.model";
import IMsg from "../models/msg.model";

export enum MsgActionType {
  MSG_WRITE = 'MSG_WRITE',
  MSG_SELECT_USER = 'MSG_SELECT_USER',
  MSG_START_TYPE = 'MSG_START_TYPE',
  MSG_STOP_TYPE = 'MSG_STOP_TYPE',
}

export const SelectUserAction: ActionCreator = createAction(
  '[WRITE] select user',
  props<{payload: IUser}>()
);

export const StartWritingAction: ActionCreator = createAction(
  '[WRITE] start writing',
  props<{payload: IUser}>()
);

export const StopWritingAction: ActionCreator = createAction(
  '[WRITE] stop writing',
  props<{payload: IUser}>()
);

export const SendMsgAction: ActionCreator = createAction(
  '[WRITE] stop writing',
  props<{payload: IMsg}>()
);
