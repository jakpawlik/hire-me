import {  createAction, props, ActionCreator } from '@ngrx/store';
import INickname from '../models/nickname.model';

export enum NicknameActionType {
  SET_NICKNAME = 'SET_NICKNAME',
}

export const SetNicknameAction: ActionCreator = createAction(
  '[CONFIG] Set nickname',
  props<{payload: INickname}>()
);
