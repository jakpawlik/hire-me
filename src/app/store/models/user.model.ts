import INickname from "./nickname.model";

export default interface IUser {
  id: string
  writing: boolean
  profileData: INickname
}

