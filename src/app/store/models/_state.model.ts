import INickname from './nickname.model';
import IChannels from "./channels.model";

export default interface State {
  readonly nickname: INickname;
  readonly msg: IChannels;
}
