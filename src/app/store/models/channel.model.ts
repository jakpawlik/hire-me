import IMsg from "./msg.model";

export default interface IChannel {
  id: string,
  messages: IMsg[]
}

