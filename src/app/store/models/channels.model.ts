import IChannel from "./channel.model";
import IUser from "./user.model";

export default interface IChannels {
  channels: {
    [key: string]: IChannel
  }
  selectedChatUser: IUser | null
}

