import IUser from "./user.model";

export default interface IMsg {
  content: string,
  createdAt: Date,
  createdBy: IUser | null
}

