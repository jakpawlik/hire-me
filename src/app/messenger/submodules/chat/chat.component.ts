import {
  Component,
  Input,
  OnInit,
  ViewChild,
  ViewChildren,
  ElementRef,
  QueryList,
  AfterViewChecked
} from '@angular/core';
import {Store} from "@ngrx/store";
import AppState from "../../../store/models/_state.model";
import IMsg from "../../../store/models/msg.model";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import IUser from "../../../store/models/user.model";
import * as moment from 'moment';
import { IconSetService } from '@coreui/icons-angular';
import { cilSend } from '@coreui/icons';
import {MsgService} from "../../msg.service";
import IChannel from "../../../store/models/channel.model";

interface IForm{
  content: FormControl<string|null>
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewChecked {
  @ViewChild('scrollMe') private scroller?: ElementRef;

  @Input() currentUser?: IUser | null;
  @Input() selectedUser?: IUser | null;
  msgs: IMsg[] = [];
  nickname: string = '';
  msgForm: FormGroup<IForm>;


  constructor(private store: Store<AppState>, private formBuilder: FormBuilder, public iconSet: IconSetService, private msgService: MsgService) {
    this.msgForm = this.formBuilder.group({
      content: '',
    });

    iconSet.icons = { cilSend }
  }

  ngAfterViewInit() {

  }

  scrollToBottom(){
    if(!this.scroller){
      return;
    }
    try {
      this.scroller.nativeElement.scrollTop = this.scroller.nativeElement.scrollHeight + 300;
    } catch (err) {}
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }


  ngOnInit(): void {
    this.store.select(state => state.nickname)
      .subscribe((state) => {
        this.nickname = state.nickname || ''
      });

    this.store.select(state => state.msg)
      .subscribe((state) => {
        if(state.selectedChatUser && this.currentUser) {
          const channel_key = [this.selectedUser?.id, this.currentUser?.id].sort().join('_') + '_chat';
          const channel: IChannel = state.channels[channel_key];

          if(channel) {
            this.msgs = channel.messages;
            console.log(channel.messages)
          }
        }
      });

    this.msgService.on('chat_message', (data) => {
      this.scrollToBottom();
    });
  }

  onSend(){
    const channel_key = [this.selectedUser?.id, this.currentUser?.id].sort().join('_') + '_chat';

    if(this.selectedUser) {
      const val: string = this.msgForm.value.content || '';

      if(!val){
        alert('Enter message');
      }

      this.msgService.emit('chat_message', {
        to: this.selectedUser,
        channelKey: channel_key,
        msg: {
          content: val,
          createdBy: this.currentUser,
          createdAt: new Date()
        },
      })

      this.msgService.dispatchMessage({
        from: this.currentUser,
        channelKey: channel_key,
        msg: {
          content: val,
          createdBy: !!this.currentUser ? this.currentUser : null,
          createdAt: new Date()
        }
      }, this.store);

      this.scrollToBottom();

      this.msgForm.reset();
    }
  }

  formatDate(date: Date){
    return moment(date).format('DD.MM.YYYY H:mm:s')
  }

  sendWritingInfo(){
    const channel_key = [this.selectedUser?.id, this.currentUser?.id].sort().join('_') + '_chat';

    if(this.selectedUser) {
      this.msgService.emit('user_writing', {
        to: this.selectedUser,
        channelKey: channel_key,
        msg: {
          createdBy: this.currentUser,
          createdAt: new Date()
        },
      });
    }
  }
}
