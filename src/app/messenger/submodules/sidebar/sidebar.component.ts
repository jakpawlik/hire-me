import { Component, OnInit, Input } from '@angular/core';
import {Store} from "@ngrx/store";
import AppState from "../../../store/models/_state.model";
import IUser from "../../../store/models/user.model";
import {MsgService} from "../../msg.service";

import { IconSetService } from '@coreui/icons-angular';
import { cilChatBubble } from '@coreui/icons';
import {MsgActionType} from "../../../store/actions/messenging.actions";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() currentUser?: IUser | null;
  @Input() selectedUser?: IUser | null;
  @Input() users: IUser[] = [];

  constructor(private msgService: MsgService, private store: Store<AppState>) {
  }

  onSelect(e: Event, user: IUser){
    e.preventDefault();
    console.log(user)
    this.store.dispatch({ type: MsgActionType.MSG_SELECT_USER, payload: user});
  }

  ngOnInit(): void {
    this.msgService.updateUsers((user) => {

      if(user && ((this.currentUser && user.id !== this.currentUser.id && user.profileData && user.profileData.nickname) && !this.users.find((item: IUser) => item.id === user.id))) {
        this.users.push(user);
      }
    }, (user) => {
      this.users = this.users.filter((item:IUser) => item.id !== user.id);
    })
  }

}
