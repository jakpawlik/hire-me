import { Socket } from 'ngx-socket-io';
import {Injectable} from "@angular/core";
import IMsg from "../store/models/msg.model";
import { map } from 'rxjs/operators';
import IUser from "../store/models/user.model";
import {MsgActionType} from "../store/actions/messenging.actions";
import {Store} from "@ngrx/store";
import AppState from "../store/models/_state.model";

@Injectable({
  providedIn: 'root'
})
class MsgService {
  connected: boolean = false;

  constructor(public socket: Socket) {
    this.init();
  }

  on(type: string, callback: (data: any) => void){
    this.socket.on(type, callback);
  }

  emit(type: string, data: any){
    this.socket.emit(type, JSON.stringify(data));
  }

  init(){
    this.socket.on('login', (data: any) => {
      this.connected = true;
    });
  }

  dispatchMessage(data: { channelKey: string, from: IUser | null | undefined, msg: IMsg }, store: Store<AppState>){
    console.log(data)
    store.dispatch({
      type: MsgActionType.MSG_WRITE,
      payload: {
        key: data.channelKey,
        data: data.msg,
        from: data.msg.createdBy,
      },
    })
  }

  sendMessage(msg: IMsg, channelKey: string) {
    this.emit(`channel_msg`, {msg: msg, channel: channelKey});
  }

  updateUsers(addCallback: (user: IUser) => void, removeCallback: (user: IUser) => void) {
    this.socket.on('welcome_party', (data: any) => {
      addCallback(JSON.parse(data))
    })

    this.socket.on('disconnected', (data: any) => {
      removeCallback(JSON.parse(data))
    })
  }

  reconnect() {
    this.socket.disconnect();
    this.socket.connect();

  }
}

export { MsgService }
