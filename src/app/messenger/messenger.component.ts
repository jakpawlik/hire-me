import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import AppState from "../store/models/_state.model";
import {ChatComponent} from './submodules/chat/chat.component';
import {SidebarComponent} from './submodules/sidebar/sidebar.component';
import { Router } from '@angular/router';
import IUser from "../store/models/user.model";
import { v1 as uniqid } from 'uuid';
import {MsgService} from "./msg.service";
import {MsgActionType} from "../store/actions/messenging.actions";

@Component({
  selector: 'app-messenger',
  templateUrl: './messenger.component.html',
  styleUrls: ['./messenger.component.scss']
})
class MessengerComponent implements OnInit {
  nickname: string = '';
  currentUser: IUser | null = null;
  selectedUser: IUser | null = null;
  users: IUser[] = [];
  writingTimeouts: {[key: string] : any} = {};

  constructor(private store: Store<AppState>, private router: Router, private msgService: MsgService) {
  }


  ngOnInit(): void {

    this.msgService.updateUsers((user) => {}, (user) => {
      if(user.id === this.selectedUser?.id){
        this.selectedUser = null;
      }
    })

    this.store.select(state => state.nickname)
      .subscribe((state) => {
        this.nickname = state.nickname || ''

        if(this.nickname === ''){
          this.router.navigate(['nickname-setup'])
        }else{
          this.msgService.on('login', (data: any) => {
            data = JSON.parse(data);

            this.currentUser = {
              id: data.userId,
              writing: false,
              profileData: {
                nickname: this.nickname
              }
            }

            this.users = data.users.filter((item: IUser) => item.profileData && item.profileData.nickname && item.profileData.nickname !== '')

            this.msgService.emit('nickname_setup', this.nickname);
          });
        }
      });

    this.store.select(state => state.msg)
      .subscribe((state) => {
        if(!this.selectedUser || (this.selectedUser && state.selectedChatUser?.id !== this.selectedUser.id)) {
          this.selectedUser = state.selectedChatUser
        }
      });

    this.msgService.on('chat_message', (data) => {
      this.msgService.dispatchMessage(data, this.store);
    });

    this.msgService.on('user_writing', (data) => {
      this.users = [...this.users.map((item) => {
        item = {...item};
        if(item.id === data.createdBy.id){
          console.log('writing')
          item.writing = true;
          clearTimeout(this.writingTimeouts[data.createdBy.id])
        }

        return item;
      })];

      this.writingTimeouts[data.createdBy.id] = setTimeout(() => {
        this.users = [...this.users.map((item) => {
          item = {...item};

          if(item.id === data.createdBy.id){
            item.writing = false;
          }

          return item;
        })];
      }, 3000);
    });
  }

}

export {
  SidebarComponent,
  ChatComponent,
  MessengerComponent
}
